package service;

import dao.AuthorDao;
import model.Author;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.io.Serializable;
import java.util.List;

public class AuthorService {


    AuthorDao authorDao = new AuthorDao();

    public List<Author> findAll() {
        return authorDao.findAll();
    }

    public Author findById(int id) {
        return authorDao.findById(id);
    }


    public int create(Author author) {
        int result = authorDao.create(author);
        return result;
    }

    public void update(Author author, Author authorToBeChanged) {
        authorDao.update(author, authorToBeChanged);
    }
    public void update(Author author){
        authorDao.update(author);
    }

    public void delete(Author author) {
        authorDao.delete(author);
    }



}
