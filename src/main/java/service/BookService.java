package service;

import dao.AuthorDao;
import dao.BookDao;
import dao.ReviewDao;
import model.Author;
import model.Book;
import model.Review;
import org.hibernate.Session;
import utils.HibernateUtils;

import java.util.ArrayList;
import java.util.List;

public class BookService {


    BookDao bookDao = new BookDao();
    ReviewDao reviewDao = new ReviewDao();
    AuthorDao authorDao = new AuthorDao();

    public Book findById(int id) {
        return bookDao.findById(id);
    }

    public List<Book> findAll() {
        return bookDao.findAll();
    }



    public int create(Book book) {
        int result = bookDao.create(book);
        return result;
    }

    public void update(Book book, Book bookToBeChanged) {
        bookDao.update(book, bookToBeChanged);
    }

    public void update(Book book) {
        bookDao.update(book);
    }

    public void delete(Book book) {
        bookDao.delete(book);
    }

    public void assignReviewToBook(int reviewId, int bookId) {
        Book b = bookDao.findById(bookId);
        Review r = reviewDao.findById(reviewId);
        List<Review> reviewList = new ArrayList<>();
        reviewList.add(r);
        b.setReview(reviewList);
        r.setBook(b);
        bookDao.update(b);
    }

    public void assignReviewToBook(Review review, int bookId) {
        Book b = bookDao.findById(bookId);
        List<Review> reviewList = new ArrayList<>();
        reviewList.add(review);
        b.setReview(reviewList);
        review.setBook(b);
        reviewDao.create(review);
        bookDao.update(b);
    }

    public void assignAuthorToBook(int authorId, int bookId) {

        Book b = bookDao.findById(bookId);
        Author a = authorDao.findById(authorId);
        b.setAuthor(a);
        bookDao.update(b);

    }
}
