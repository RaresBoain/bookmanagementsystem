package service;

import dao.BookDao;
import dao.ReviewDao;
import model.Author;
import model.Book;
import model.Review;
import org.hibernate.Session;
import utils.HibernateUtils;

import java.util.ArrayList;
import java.util.List;

public class ReviewService {


    ReviewDao reviewDao = new ReviewDao();
    BookDao bookDao = new BookDao();


    public Review findById(int id) {
        return reviewDao.findById(id);
    }

    public List<Review> findAll() {
        return reviewDao.findAll();
    }


    public int create(Review review) {
        int result = reviewDao.create(review);
        return result;
    }

    public void update(Review review) {
        reviewDao.update(review);
    }

    public void update(Review review, Review reviewToBeChanged) {
        reviewDao.update(review, reviewToBeChanged);
    }

    public void delete(Review review) {
        reviewDao.delete(review);
    }
}
